package com.exelcia.formation;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
//import org.junit.BeforeEach;
import com.exelcia.formation.App;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import java.time.Duration;


/**
 * Unit test for simple App.
 */
public class AppTest 
{

    //private App app;
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    /*@BeforeEach
    public void newApp()
    {
        app = new App();
    }*/

    @Test
    public void testadditionOK()
    {
       App app = new App();
        Integer result = app.additionner(5,6);
        if(result == 11)
           assertTrue( true );
        else
           assertTrue( false );
    }

    @Test
    public void testSelenium()
    {
        ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
		options.addArguments("--no-sandbox"); // Bypass OS security model
		options.addArguments("--disable-extensions"); // disabling extensions
		options.addArguments("--headless"); // Headless
         WebDriver driver = new ChromeDriver(options);
         WebDriverWait wait = new WebDriverWait(driver, 10L);
        try{
        // Optional. If not specified, WebDriver searches the PATH for chromedriver.
            driver.get("https://google.com/ncr");
            driver.findElement(By.name("q")).sendKeys("cheese" + Keys.ENTER);
            WebElement firstResult = wait.until(presenceOfElementLocated(By.cssSelector("h3")));
            System.out.println(firstResult.getAttribute("textContent"));           
        }
        finally {
            driver.quit();
        }
    }
}
